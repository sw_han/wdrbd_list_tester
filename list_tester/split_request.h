#ifndef _SPLIT_REQUEST_
#define _SPLIT_REQUEST_

#include "resync_pending.h"

static atomic_t g_unmarked_count = 0;
static atomic_t g_failed_unmarked = 0;
static atomic_t g_count = 0;

int list_add_marked(struct drbd_peer_device* peer_device, sector_t sst, sector_t est, unsigned int size, bool in_sync);
bool is_out_of_sync_after_replication(struct drbd_device *device, ULONG_PTR s_bb, ULONG_PTR e_next_bb);
bool is_marked_rl_bb(struct drbd_peer_device *peer_device, struct drbd_marked_replicate **marked_rl, ULONG_PTR bb);
bool prepare_split_peer_request(struct drbd_peer_device *peer_device, ULONG_PTR s_bb, ULONG_PTR e_next_bb, atomic_t *split_count, ULONG_PTR* e_oos);
bool check_unmarked_and_processing(struct drbd_peer_device *peer_device, struct drbd_peer_request *peer_req);
int split_request_complete(struct drbd_peer_device* peer_device, struct drbd_peer_request *peer_req);
int split_e_end_resync_block(struct drbd_peer_request *peer_req);
void split_read_in_block(struct drbd_peer_device* peer_device, char* type, sector_t sector, ULONG_PTR offset, unsigned int size, ULONG_PTR s_bb, ULONG_PTR e_next_bb, ULONG_PTR flags);
int split_recv_resync_read(struct drbd_peer_device *peer_device, sector_t sector, unsigned int size);
int drbd_bm_test_bit(struct drbd_peer_device* peer_device, ULONG_PTR bit);

#endif _SPLIT_REQUEST_