#ifndef _RESYNC_PENDING_
#define _RESYNC_PENDING_

#include "common.h"

bool get_resync_pending_range(struct drbd_peer_device* peer_device, sector_t sst, sector_t est, sector_t *cst);
void dup_verification_and_processing(struct drbd_peer_device* peer_device, struct drbd_peer_request *peer_req);
struct drbd_resync_pending_sectors *resync_pending_check_and_expand_dup(struct drbd_device* device, sector_t sst, sector_t est);
void resync_pending_list_all_check_and_dedup(struct drbd_device* device, struct drbd_resync_pending_sectors *pending_st);
int list_add_resync_pending(struct drbd_device* device, sector_t sst, sector_t est);
int dedup_from_resync_pending(struct drbd_peer_device *peer_device, sector_t sst, sector_t est);

#endif