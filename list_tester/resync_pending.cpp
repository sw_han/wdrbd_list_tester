#include "resync_pending.h"

extern FILE* g_fp;

bool get_resync_pending_range(struct drbd_peer_device* peer_device, sector_t sst, sector_t est, sector_t *cst)
{
	struct drbd_resync_pending_sectors *target = NULL;

	mutex_lock(&peer_device->device->resync_pending_fo_mutex);
	list_for_each_entry(struct drbd_resync_pending_sectors, target, &(peer_device->device->resync_pending_sectors), pending_sectors) {
		if (est <= target->sst) {
			// all ranges are not duplicated with resync pending
			// cst (current sector)
			*cst = est;
			mutex_unlock(&peer_device->device->resync_pending_fo_mutex);
			// return false not duplicate
			return false;
		}
		else if ((sst >= target->sst && est <= target->est)) {
			// all ranges are duplicated with resync pending
			drbd_info(peer_device, "dup all out of sync sectors %llu ~ %llu => source %llu ~ %llu, target %llu ~ %llu\n",
				(unsigned long long)sst, (unsigned long long)est, (unsigned long long)sst, (unsigned long long)est, (unsigned long long)target->sst, (unsigned long long)target->est);
			*cst = est;
			mutex_unlock(&peer_device->device->resync_pending_fo_mutex);
			// return true duplicate 
			return true;
		}
		else if (sst < target->sst && est > target->sst) {
			// not all ranges duplicated
			*cst = target->sst;
			mutex_unlock(&peer_device->device->resync_pending_fo_mutex);
			return false;
		}
		else if (sst < target->sst && (est <= target->est || est >= target->est)) {
			// front range duplicated
			drbd_info(peer_device, "dup front out of sync sectors %llu ~ %llu => source %llu ~ %llu, target %llu ~ %llu\n",
				(unsigned long long)target->sst, (unsigned long long)(target->est < est ? target->est : est), (unsigned long long)sst, (unsigned long long)est, (unsigned long long)target->sst, (unsigned long long)target->est);
			*cst = (target->est < est ? target->est : est);
			mutex_unlock(&peer_device->device->resync_pending_fo_mutex);
			return true;
		}
		else if (sst >= target->sst && sst < target->est && est > target->est) {
			// end range duplicated 
			drbd_info(peer_device, "dup end out of sync sectors %llu ~ %llu => source %llu ~ %llu, target %llu ~ %llu\n",
				(unsigned long long)sst, (unsigned long long)target->est, (unsigned long long)sst, (unsigned long long)est, (unsigned long long)target->sst, (unsigned long long)target->est);
			*cst = target->est;
			mutex_unlock(&peer_device->device->resync_pending_fo_mutex);
			return true;
		}
	}
	*cst = est;
	mutex_unlock(&peer_device->device->resync_pending_fo_mutex);

	return false;
}

void dup_verification_and_processing(struct drbd_peer_device* peer_device, struct drbd_peer_request *peer_req)
{
	sector_t sst, offset, est = peer_req->i.sector + (peer_req->i.size >> 9);
	struct drbd_peer_device* tmp = NULL;
	enum drbd_packet cmd = P_RS_WRITE_ACK;

	sst = offset = peer_req->i.sector;

	while (offset < est && offset >= sst) {
		if (!get_resync_pending_range(peer_device, offset, est, &offset)) {
			// return false indicates ranges in sync.
			for_each_peer_device(tmp, peer_device->device) {
				if (tmp == peer_device ||
					tmp->current_uuid == peer_device->current_uuid) {

					printf("drbd_set_in_sync() sst(%llu), offset(%llu), size(%u)\n", sst, offset, (int)(offset - sst) << 9);
					drbd_set_in_sync(tmp, sst, (int)(offset - sst) << 9);
					cmd = P_RS_WRITE_ACK;
				}
			}
		}
		else {
			// return true indicates ranges out of sync (duplicate ranges)
			for_each_peer_device(tmp, peer_device->device) {
				if (tmp == peer_device ||
					tmp->current_uuid == peer_device->current_uuid) {
					// DW-2058 set rs_failed
					drbd_rs_failed_io(tmp, sst, (int)(offset - sst) << 9); 

					printf("drbd_set_out_of_sync() sst(%llu), offset(%llu), size(%u)\n", sst, offset, (int)(offset - sst) << 9);
					drbd_set_out_of_sync(tmp, sst, (int)(offset - sst) << 9);
					fseek(g_fp, (sst << 9), SEEK_SET);
					for (int i = 0; i < (int)(offset - sst) << 9; i++)
						fwrite(RESYNC_PENDING_STR, 1, 1, g_fp);
					cmd = P_NEG_ACK;
				}
			}
		}

		// send the result only when it is not a split request.
		// (if it is a split request, set the bitmap only and send the result from the split_request_complet())
		if (!(peer_req->flags & EE_SPLIT_REQ) && !(peer_req->flags & EE_SPLIT_LAST_REQ)) {
			_drbd_send_ack(peer_device, cmd, cpu_to_be64(sst), cpu_to_be32((int)(offset - sst) << 9),
				((offset == est) ? ID_SYNCER_SPLIT_DONE : ID_SYNCER_SPLIT));
		}

		sst = offset;
	}
}

struct drbd_resync_pending_sectors *resync_pending_check_and_expand_dup(struct drbd_device* device, sector_t sst, sector_t est)
{
	struct drbd_resync_pending_sectors *pending_st = NULL;

	if (list_empty(&device->resync_pending_sectors))
		return NULL;

	list_for_each_entry(struct drbd_resync_pending_sectors, pending_st, &(device->resync_pending_sectors), pending_sectors) {
		if (sst >= pending_st->sst && sst <= pending_st->est && est <= pending_st->est) {
			// ignore them because they already have the all rangs.
			return pending_st;
		}

		if (sst <= pending_st->sst && est >= pending_st->sst && est > pending_st->est) {
			// update sst and est because it contains a larger range that already exists.
			pending_st->sst = sst;
			pending_st->est = est;
			return pending_st;
		}

		if (sst >= pending_st->sst && sst < pending_st->est && est > pending_st->est) {
			// existing ranges include start ranges, but end ranges are larger, so update the est values.
			pending_st->est = est;
			return pending_st;
		}

		if (sst < pending_st->sst && est > pending_st->sst && est <= pending_st->est) {
			// existing ranges include end ranges, but start ranges are small, so update the sst values.
			pending_st->sst = sst;
			return pending_st;
		}
	}
	// there is no equal range.
	return NULL;
}

// DW-2042 if you already have a range, remove the duplicate entry. (all list item)
void resync_pending_list_all_check_and_dedup(struct drbd_device* device, struct drbd_resync_pending_sectors *pending_st)
{
	struct drbd_resync_pending_sectors *target, *tmp;

	list_for_each_entry_safe(struct drbd_resync_pending_sectors, target, tmp, &(device->resync_pending_sectors), pending_sectors) {
		if (pending_st == target)
			continue;

		if (pending_st->sst <= target->sst && pending_st->est >= target->est) {
			// remove all ranges as they are included.
			list_del(&target->pending_sectors);
			kfree2(target);
			continue;
		}
		if (pending_st->sst > target->sst && pending_st->sst <= target->est) {
			// the end range is included, so update the est.
			target->est = pending_st->sst;
		}

		if (pending_st->sst <= target->sst && pending_st->est > target->sst) {
			// the start range is included, so update the sst.
			target->sst = pending_st->est;
		}
	}
}

int list_add_resync_pending(struct drbd_device* device, sector_t sst, sector_t est)
{
	struct drbd_resync_pending_sectors *pending_st = NULL;
	struct drbd_resync_pending_sectors *target = NULL;

	int i = 0;

	// remove duplicates from items you want to add.
	mutex_lock(&device->resync_pending_fo_mutex);
	pending_st = resync_pending_check_and_expand_dup(device, sst, est);
	if (pending_st) {
		resync_pending_list_all_check_and_dedup(device, pending_st);
	}
	else {
		struct drbd_resync_pending_sectors *target;

		pending_st = ExAllocatePoolWithTag(drbd_resync_pending_sectors, sizeof(struct drbd_resync_pending_sectors), 'E9DW');
		if (!pending_st) {
			drbd_err(device, "failed to resync pending bits allocate, sector : %llu ~ %llu\n", (unsigned long long)sst, (unsigned long long)est);
			mutex_unlock(&device->resync_pending_fo_mutex);
			return -ENOMEM;
		}

		pending_st->sst = sst;
		pending_st->est = est;

		// add to the list in sequential sort.
		if (list_empty(&device->resync_pending_sectors)) {
			list_add(&pending_st->pending_sectors, &device->resync_pending_sectors);
		}
		else {
			list_for_each_entry(struct drbd_resync_pending_sectors, target, &(device->resync_pending_sectors), pending_sectors) {
				if (pending_st->sst < target->sst) {
					if (device->resync_pending_sectors.next == &target->pending_sectors)
						list_add(&pending_st->pending_sectors, &device->resync_pending_sectors);
					else
						list_add_tail(&pending_st->pending_sectors, &target->pending_sectors);

					goto eof;
				}
			}
			list_add_tail(&pending_st->pending_sectors, &device->resync_pending_sectors);
		}
	}
eof:
	list_for_each_entry(struct drbd_resync_pending_sectors, target, &(device->resync_pending_sectors), pending_sectors)
		drbd_info(device, "%d. resync pending sector %llu(%llu) ~ %llu(%llu)\n", i++, (unsigned long long)target->sst, (unsigned long long)BM_SECT_TO_BIT(target->sst), (unsigned long long)target->est, (unsigned long long)BM_SECT_TO_BIT(target->est));
	mutex_unlock(&device->resync_pending_fo_mutex);

	return 0;
}

int dedup_from_resync_pending(struct drbd_peer_device *peer_device, sector_t sst, sector_t est)
{
	struct drbd_resync_pending_sectors *target, *tmp;

	mutex_lock(&peer_device->device->resync_pending_fo_mutex);
	list_for_each_entry_safe(struct drbd_resync_pending_sectors, target, tmp, &(peer_device->device->resync_pending_sectors), pending_sectors) {
		if (sst <= target->sst && est >= target->est) {
			drbd_info(peer_device, "resync pending remove sector %llu(%llu) ~ %llu(%llu)\n",
				(unsigned long long)target->sst, (unsigned long long)BM_SECT_TO_BIT(target->sst), (unsigned long long)target->est, (unsigned long long)BM_SECT_TO_BIT(target->est));
			// remove because it contains the full range
			list_del(&target->pending_sectors);
			kfree2(target);
			continue;
		}

		if (sst >= target->sst && est <= target->est) {
			// remove because the middle range is the same
			struct drbd_resync_pending_sectors *pending_st;

			// adding it to the list because it will disperse when the middle is removed
			pending_st = ExAllocatePoolWithTag(drbd_resync_pending_sectors, sizeof(struct drbd_resync_pending_sectors), 'E9DW');
			if (!pending_st) {
				drbd_err(peer_device, "failed to resync pending bits allocate, sector : %llu ~ %llu\n", (unsigned long long)sst, (unsigned long long)est);
				mutex_unlock(&peer_device->device->resync_pending_fo_mutex);
				return -ENOMEM;
			}

			pending_st->sst = est;
			pending_st->est = target->est;
			list_add(&pending_st->pending_sectors, &target->pending_sectors);
			drbd_info(peer_device, "resync pending split new sector %llu(%llu) ~ %llu(%llu)\n",
				(unsigned long long)pending_st->sst, (unsigned long long)BM_SECT_TO_BIT(pending_st->sst), (unsigned long long)pending_st->est, (unsigned long long)BM_SECT_TO_BIT(pending_st->est));

			target->est = sst;
			drbd_info(peer_device, "resync pending split sector %llu(%llu) ~ %llu(%llu)\n",
				(unsigned long long)target->sst, (unsigned long long)BM_SECT_TO_BIT(target->sst), (unsigned long long)target->est, (unsigned long long)BM_SECT_TO_BIT(target->est));
		}

		if (sst <= target->sst && est > target->sst && est <= target->est) {
			// remove because the start range is the same.
			target->sst = est;
			drbd_info(peer_device, "resync pending modify sector %llu(%llu) ~ %llu(%llu)\n",
				(unsigned long long)target->sst, (unsigned long long)BM_SECT_TO_BIT(target->sst), (unsigned long long)target->est, (unsigned long long)BM_SECT_TO_BIT(target->est));
		}

		if (sst >= target->sst && sst < target->est && est >= target->est) {
			// remove because the end range is the same.
			target->est = sst;
			drbd_info(peer_device, "resync pending modify sector %llu(%llu) ~ %llu(%llu)\n",
				(unsigned long long)target->sst, (unsigned long long)BM_SECT_TO_BIT(target->sst), (unsigned long long)target->est, (unsigned long long)BM_SECT_TO_BIT(target->est));
		}
	}
	mutex_unlock(&peer_device->device->resync_pending_fo_mutex);

	return 0;
}