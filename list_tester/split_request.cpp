#include "split_request.h"

extern FILE *g_fp;
extern unsigned int g_bits;

int list_add_marked(struct drbd_peer_device* peer_device, sector_t sst, sector_t est, unsigned int size, bool in_sync)
{
	ULONG_PTR s_bb, e_bb;
	struct drbd_device* device = peer_device->device;

	s_bb = (ULONG_PTR)BM_SECT_TO_BIT(sst);
	e_bb = (ULONG_PTR)BM_SECT_TO_BIT(est);

	//DW-1911 use e_bb instead of e_next_b for replication.
	if (BM_BIT_TO_SECT(e_bb) == est)
		e_bb -= 1;

	ULONG_PTR s_resync_bb = peer_device->device->s_resync_bb;
	ULONG_PTR n_resync_bb = peer_device->device->e_resync_bb;

	struct drbd_marked_replicate *marked_rl = NULL, *s_marked_rl = NULL, *e_marked_rl = NULL;

	if ((s_resync_bb <= e_bb && n_resync_bb >= e_bb) ||
		(s_resync_bb <= s_bb && n_resync_bb >= s_bb)) {
		//DW-1911 check if marked already exists.
		list_for_each_entry(struct drbd_marked_replicate, marked_rl, &(device->marked_rl_list), marked_rl_list) {
			if (marked_rl->bb == s_bb)
				s_marked_rl = marked_rl;
			if (marked_rl->bb == e_bb)
				e_marked_rl = marked_rl;

			if (s_marked_rl && e_marked_rl)
				break;
		}

		//printf("BM_BIT_TO_SECT(s_bb)%llu != sst%llu || (BM_BIT_TO_SECT(s_bb)%llu == sst%llu && s_bb%llu == e_bb%llu), test_bit %d\n", BM_BIT_TO_SECT(s_bb), sst, BM_BIT_TO_SECT(s_bb), sst, s_bb, e_bb, drbd_bm_test_bit(peer_device, s_bb));
		if ((BM_BIT_TO_SECT(s_bb) != sst || (BM_BIT_TO_SECT(s_bb) == sst && s_bb == e_bb)) &&
			drbd_bm_test_bit(peer_device, s_bb) == 1) {
			if (!s_marked_rl) {
				s_marked_rl = ExAllocatePoolWithTag(drbd_marked_replicate, sizeof(struct drbd_marked_replicate), 'E8DW');
				if (s_marked_rl != NULL) {
					s_marked_rl->bb = s_bb;
					s_marked_rl->marked_rl = 0;
					s_marked_rl->end_unmarked_rl = 0;
					list_add(&(s_marked_rl->marked_rl_list), &device->marked_rl_list);
				}
				else {
					drbd_err(peer_device, "failed to marked replicate allocate, bit : %llu\n", (unsigned long long)s_bb);
					return -ENOMEM;
				}
			}

			//DW-1911 set the bit to match the sector.
			unsigned short offset = (unsigned short)(sst - BM_BIT_TO_SECT(s_bb));
			unsigned short i;
			fseek(g_fp, sst << 9, SEEK_SET);

			for (i = offset; i < (offset + (size >> 9)); i++) {
				if (BM_SECT_TO_BIT(BM_BIT_TO_SECT(s_bb) + i) != s_bb)
					break;
				s_marked_rl->marked_rl |= 1 << i;
				for (int j = 0; j < (1 << 9); j++) 
					fwrite(RESYNC_MARK_BIT_STR, 1, 1, g_fp);
			}
			drbd_info(peer_device, "sbb marking bb(%llu), ssector(%llu), sector(%llu), count(%u), size(%u), marked(%u), offset(%u)\n",
				(unsigned long long)s_marked_rl->bb, (unsigned long long)sst, (unsigned long long)BM_BIT_TO_SECT(s_marked_rl->bb), (i - offset), (size >> 9), s_marked_rl->marked_rl, offset);
		}

		//printf("s_bb%llu != e_bb%llu && BM_BIT_TO_SECT(BM_SECT_TO_BIT(est))%llu != est%llu, test_bit %d\n", s_bb, e_bb, BM_BIT_TO_SECT(BM_SECT_TO_BIT(est)), est, drbd_bm_test_bit(peer_device, e_bb));
		if (s_bb != e_bb && BM_BIT_TO_SECT(BM_SECT_TO_BIT(est)) != est && drbd_bm_test_bit(peer_device, e_bb) == 1) {
			if (!e_marked_rl) {
				e_marked_rl = ExAllocatePoolWithTag(drbd_marked_replicate, sizeof(struct drbd_marked_replicate), 'E8DW');
				if (e_marked_rl != NULL) {
					e_marked_rl->bb = e_bb;
					e_marked_rl->marked_rl = 0;
					e_marked_rl->end_unmarked_rl = 0;
					list_add(&(e_marked_rl->marked_rl_list), &device->marked_rl_list);
				}
				else {
					drbd_err(peer_device, "failed to marked replicate allocate, bit : %llu\n", (unsigned long long)e_bb);
					return -ENOMEM;
				}
			}

			unsigned short i;

			fseek(g_fp, (BM_BIT_TO_SECT(e_bb) << 9), SEEK_SET);
			//DW-1911 set the bit to match the sector.
			for (i = 0; i < (est - BM_BIT_TO_SECT(e_bb)); i++) {
				e_marked_rl->marked_rl |= 1 << i;
				for (int j = 0; j < (1 << 9); j++)
					fwrite(RESYNC_MARK_BIT_STR, 1, 1, g_fp);
			}
			drbd_info(peer_device, "ebb marking bb(%llu), esector(%llu), sector(%llu), count(%u), size(%u), marked(%u), offset(%u)\n",
				(unsigned long long)e_marked_rl->bb, est, (unsigned long long)BM_BIT_TO_SECT(e_marked_rl->bb), i, (size >> 9), e_marked_rl->marked_rl, 0);
		}
	}

	//DW-1904 this area is set up to check marked_rl and in sync bit when receiving resync data.
	if (in_sync) {
		//DW-1911 marked_rl bit is excluded.
		if (s_marked_rl != NULL)
			s_bb += 1;
		if (e_marked_rl != NULL)
			e_bb -= 1;

		if (device->s_rl_bb > s_bb)
			device->s_rl_bb = s_bb;
		if (device->e_rl_bb < e_bb)
			device->e_rl_bb = e_bb;
	}


	return 0;
}


bool is_out_of_sync_after_replication(struct drbd_device *device, ULONG_PTR s_bb, ULONG_PTR e_next_bb)
{
	// DW-1904 check that the resync data is within the out of sync range of the replication data.
	if (device->e_rl_bb >= device->s_rl_bb) {
		// DW-2065
		if ((device->s_rl_bb <= s_bb && device->e_rl_bb >= s_bb)) {
			if (device->e_rl_bb <= (e_next_bb - 1)) {
				printf("# s_rl_bb(%llu) => %llu, e_rl_bb(%llu) => %llu\n", device->s_rl_bb, device->s_rl_bb, device->e_rl_bb, s_bb - 1);
				//device->e_rl_bb = (s_bb - 1);
			}
			return true;
		}

		if (device->s_rl_bb <= (e_next_bb - 1) && device->e_rl_bb >= (e_next_bb - 1)) {
			if (device->s_rl_bb >= s_bb) {
				printf("## s_rl_bb(%llu) => %llu, e_rl_bb(%llu) => %llu\n", device->s_rl_bb, e_next_bb, device->e_rl_bb, device->e_rl_bb);
				//device->s_rl_bb = e_next_bb;
			}
			return true;
		}

		if ((device->s_rl_bb >= s_bb && device->e_rl_bb <= (e_next_bb - 1))) {
			printf("### s_rl_bb(%llu) => %llu, e_rl_bb(%llu) => %llu\n", device->s_rl_bb, UINT64_MAX, device->e_rl_bb, (unsigned long long)0);
			//device->s_rl_bb = UINT64_MAX;
			//device->e_rl_bb = 0;
			return true;
		}
	}

	return false;
}


bool is_marked_rl_bb(struct drbd_peer_device *peer_device, struct drbd_marked_replicate **marked_rl, ULONG_PTR bb)
{
	list_for_each_entry(struct drbd_marked_replicate, (*marked_rl), &(peer_device->device->marked_rl_list), marked_rl_list) {
		if ((*marked_rl)->bb == bb) {
			return true;
		}
	}

	(*marked_rl) = NULL;
	return false;
}


bool prepare_split_peer_request(struct drbd_peer_device *peer_device, ULONG_PTR s_bb, ULONG_PTR e_next_bb, atomic_t *split_count, ULONG_PTR* e_oos)
{
	bool find_isb = false;
	bool split_request = true;
	struct drbd_marked_replicate *marked_rl, *tmp;
	ULONG_PTR sibb = 0;

	//DW-1911 
	list_for_each_entry_safe(struct drbd_marked_replicate, marked_rl, tmp, &(peer_device->device->marked_rl_list), marked_rl_list) {
		//DW-1911 set in sync if all the sector are marked.
		if (__popcnt(marked_rl->marked_rl) == (sizeof(marked_rl->marked_rl) * 8)) {
			printf("all replication mark bit, set in sync(%u)\n", marked_rl->bb);
			drbd_set_in_sync(peer_device, BM_BIT_TO_SECT(marked_rl->bb), BM_SECT_PER_BIT << 9);
			list_del(&(marked_rl->marked_rl_list));

			g_bits ^= (1 << marked_rl->bb);
			fseek(g_fp, (BM_BIT_TO_SECT(marked_rl->bb) << 9), SEEK_SET);
			for (int i = 0; i < (1 << BM_BLOCK_SHIFT); i++)
				fwrite(IN_SYNC_STR, 1, 1, g_fp);
			kfree2(marked_rl);
			continue;
		}

		//DW-1911 if it is already in sync bit, remove it.
		if (drbd_bm_test_bit(peer_device, marked_rl->bb) == 0) {
			list_del(&marked_rl->marked_rl_list);
			kfree2(marked_rl);
			continue;
		}
	}

	//DW-1601 the last out of sync and split_cnt information are obtained before the resync write request.
	for (ULONG_PTR ibb = s_bb; ibb < e_next_bb; ibb++) {
		// DW-1928 modify split_count calculation method
		if (is_marked_rl_bb(peer_device, &marked_rl, ibb)) {
			for (u16 i = 0; i < sizeof(marked_rl->marked_rl) * 8; i++) {
				// DW-1911 obtain the end unmakred sector.
				if (!(marked_rl->marked_rl & 1 << i)) {
					if (marked_rl->end_unmarked_rl < i)
						marked_rl->end_unmarked_rl = i;

					printf("marked rl bb %llu, sector %u\n", ibb, i);
					atomic_inc(split_count);
				}
			}
			if (!split_request)
				printf("split request %llu ~ %llu\n", sibb, ibb);
			split_request = true;
			find_isb = true;

			*e_oos = ibb;
		}
		else if (drbd_bm_test_bit(peer_device, ibb) == 1) {
			if (split_request) {
				sibb = ibb;
				atomic_inc(split_count);
				split_request = false;
			}

			if (ibb == (e_next_bb - 1))
				printf("split request %llu ~ %llu\n", sibb, ibb);

			*e_oos = ibb;
		}
		else {
			//drbd_info(peer_device, "##find in sync bitmap bit : %llu, start (%llu) ~ end (%llu)\n",
			//	(unsigned long long)ibb,
			//	(unsigned long long)s_bb,
			//	(unsigned long long)(e_next_bb - 1));

			if (!split_request)
				printf("split request %llu ~ %llu\n", sibb, ibb);
			split_request = true;
			find_isb = true;

		}
	}

	return find_isb;
}

bool check_unmarked_and_processing(struct drbd_peer_device *peer_device, struct drbd_peer_request *peer_req)
{
	bool unmakred = peer_req->unmarked_count != NULL;
	sector_t sector = peer_req->i.sector;

	if (peer_req->unmarked_count && *peer_req->unmarked_count > 0) {
		printf("unmakred sector(%llu), size(%lu), cnt(%u), unmarked_cnt(%u)\n", peer_req->i.sector, peer_req->i.size, *peer_req->count, *peer_req->unmarked_count);
	}

	if (peer_req->unmarked_count &&
		0 == (*peer_req->unmarked_count -= 1)) {
		unmakred = false;

		//DW-1911 if there is a failure, set the EE_WAS_ERROR setting.
		if (*peer_req->failed_unmarked == 1) {
			//peer_req->flags |= EE_WAS_ERROR;
		}

		//drbd_info(peer_device, "--finished unmarked s_bb(%llu), e_bb(%llu), sector(%llu), res(%s)\n",
		//	(unsigned long long)peer_req->s_bb, (unsigned long long)(peer_req->e_next_bb - 1), sector, (*peer_req->failed_unmarked == 1 ? "failed" : "success"));

		// DW-2082
		peer_req->i.sector = BM_BIT_TO_SECT(BM_SECT_TO_BIT(peer_req->i.sector));
		peer_req->i.size = BM_SECT_PER_BIT << 9;

		kfree(peer_req->unmarked_count);
		if (peer_req->failed_unmarked)
			kfree(peer_req->failed_unmarked);
	}
	else {
		if (peer_req->flags & EE_WAS_ERROR) {
			//DW-1911 
			if (unmakred && peer_req->failed_unmarked)
				*peer_req->failed_unmarked = 1;
		}
	}

	return unmakred;
}

int split_request_complete(struct drbd_peer_device* peer_device, struct drbd_peer_request *peer_req)
{
	//true : in sync, false : out of sync
	bool is_in_sync = false;
	ULONG_PTR s_bb = peer_req->s_bb;
	ULONG_PTR e_next_bb = peer_req->e_next_bb;
	int err = 0;

	// DW-2055 resync data write complete should be set only when synctarget
	// if (is_sync_target(peer_device) || peer_device->repl_state[NOW] == L_BEHIND) {
	for (ULONG_PTR i_bb = peer_req->s_bb; i_bb < e_next_bb; i_bb++) {
		if (drbd_bm_test_bit(peer_device, i_bb) == 1) {
			if (is_in_sync == true && i_bb != peer_req->s_bb) {
			complete_end_sync:
				//DW-1601 If all of the data are sync, then P_RS_WRITE_ACK transmit.
				peer_req->i.sector = BM_BIT_TO_SECT(s_bb);
				peer_req->i.size = (unsigned int)BM_BIT_TO_SECT(i_bb - s_bb) << 9;
				//drbd_info(peer_device, "--set in sync, bitmap bit start : %llu, range : %llu ~ %llu, size %llu\n",
				//	(unsigned long long)peer_req->s_bb, (unsigned long long)s_bb, (unsigned long long)(i_bb - 1), (unsigned long long)(BM_BIT_TO_SECT(i_bb - s_bb) << 9));
				if (i_bb == e_next_bb)
					peer_req->block_id = ID_SYNCER_SPLIT_DONE;
				else
					peer_req->block_id = ID_SYNCER_SPLIT;

				//err = drbd_send_ack(peer_device, P_RS_WRITE_ACK, peer_req);
				s_bb = i_bb;
			}

			if ((i_bb + 1) == e_next_bb) {
				i_bb = e_next_bb;
				goto complete_end_out_of_sync;
			}

			is_in_sync = false;
		}
		else {
			if (is_in_sync == false && i_bb != peer_req->s_bb) {
			complete_end_out_of_sync:
				//DW-1601 If out of sync is found within range, it is set as a failure.
				peer_req->i.sector = BM_BIT_TO_SECT(s_bb);
				peer_req->i.size = (unsigned int)BM_BIT_TO_SECT(i_bb - s_bb) << 9;
				//drbd_err(peer_device, "--set failed to I/O, bitmap bit start : %llu, range : %llu ~ %llu, size %llu\n",
				//	(unsigned long long)peer_req->s_bb, (unsigned long long)s_bb, (unsigned long long)(i_bb - 1), (unsigned long long)(BM_BIT_TO_SECT(i_bb - s_bb) << 9));
				if (i_bb == e_next_bb)
					peer_req->block_id = ID_SYNCER_SPLIT_DONE;
				else
					peer_req->block_id = ID_SYNCER_SPLIT;

				//err = drbd_send_ack(peer_device, P_NEG_ACK, peer_req);
				s_bb = i_bb;
			}

			if ((i_bb + 1) == e_next_bb) {
				i_bb = e_next_bb;
				goto complete_end_sync;
			}

			is_in_sync = true;
		}
	}
	//}
	//else {
	//	peer_req->block_id = ID_SYNCER_SPLIT_DONE;
	//	peer_req->i.sector = BM_BIT_TO_SECT(peer_req->s_bb);
	//	peer_req->i.size = (unsigned int)BM_BIT_TO_SECT(peer_req->e_next_bb - peer_req->s_bb) << 9;
	//	err = drbd_send_ack(peer_device, P_NEG_ACK, peer_req);
	//}

	if (peer_req->count)
		kfree(peer_req->count);

	return err;
}

int split_e_end_resync_block(struct drbd_peer_request *peer_req)
{
	//struct drbd_peer_request *peer_req =
	//	container_of(w, struct drbd_peer_request, w);
	struct drbd_peer_device *peer_device = peer_req->peer_device;
	sector_t sector = peer_req->i.sector;
	int err = 0;
	bool is_unmarked = false;

	//drbd_info(peer_device, "--bitmap bit : %llu ~ %llu\n", BM_SECT_TO_BIT(peer_req->i.sector), (BM_SECT_TO_BIT(peer_req->i.sector + (peer_req->i.size >> 9)) - 1));

	is_unmarked = check_unmarked_and_processing(peer_device, peer_req);


	// DW-2055 resync data write complete should be set only when synctarget
	//if (is_sync_target(peer_device)) {
		if ((peer_req->flags & EE_WAS_ERROR) == 0) {
			if (!is_unmarked) {
				// DW-2042
				dup_verification_and_processing(peer_device, peer_req);
			}
		}
		else {
			if (!is_unmarked) {
				drbd_rs_failed_io(peer_device, sector, peer_req->i.size);
				if (!(peer_req->flags & EE_SPLIT_REQ) && !(peer_req->flags & EE_SPLIT_LAST_REQ)) {
					//err = drbd_send_ack(peer_device, P_NEG_ACK, peer_req);
				}
			}
		}
	//}
	//else  {
	//	drbd_set_out_of_sync(peer_device, sector, peer_req->i.size);
	//	if (!(peer_req->flags & EE_SPLIT_REQ) && !(peer_req->flags & EE_SPLIT_LAST_REQ)) {
	//		err = drbd_send_ack(peer_device, P_NEG_ACK, peer_req);
	//	}
	//}

	//
	////DW-1911 check split request
	if (peer_req->flags & EE_SPLIT_REQ || peer_req->flags & EE_SPLIT_LAST_REQ) {
		printf("peer request count %u\n", *peer_req->count);
		//DW-1911 check that all split requests are completed.
		if (peer_req->count && 0 == (*peer_req->count -= 1)) {
			//dec_unacked(peer_device);
			err = split_request_complete(peer_req->peer_device, peer_req);
		}
	}
	//else {
	//	//dec_unacked(peer_device);
	//}

	return err;
}


void split_read_in_block(struct drbd_peer_device* peer_device, char* type, sector_t sector, ULONG_PTR offset, unsigned int size, ULONG_PTR s_bb, ULONG_PTR e_next_bb, ULONG_PTR flags)
{
	struct drbd_peer_request peer_req;

	peer_req.peer_device = peer_device;

	printf("%s, sector count(%llu), offset(%llu), size(%u), s_bb(%llu), e_next(%llu), flags(%s), split_count(%u)\n", type,
		sector, offset, size, s_bb, e_next_bb, (flags == EE_SPLIT_REQ ? "EE_SPLIT_REQ" : "EE_SPLIT_LAST_REQ"), g_count);

	peer_req.i.size = size;
	peer_req.i.sector = sector;
	peer_req.flags = 0;
	peer_req.flags |= flags;
	peer_req.flags |= 1;

	peer_req.count = &g_count;
	peer_req.s_bb = s_bb;
	peer_req.e_next_bb = e_next_bb;
	peer_req.unmarked_count = &g_unmarked_count;
	peer_req.failed_unmarked = &g_failed_unmarked;

	fseek(g_fp, (peer_req.i.sector << 9), SEEK_SET);
	for (int i = 0; i < peer_req.i.size; i++) 
		fwrite(RESYNC_DATA_STR, 1, 1, g_fp);

	split_e_end_resync_block(&peer_req);
}


int split_recv_resync_read(struct drbd_peer_device *peer_device, sector_t sector, unsigned int size)
{
	struct drbd_device *device = peer_device->device;
	struct drbd_peer_request peer_req;

	int err = 0;

	ULONG_PTR s_bb, e_next_bb, e_oos; //s_bb = start bitmap bit, e_next_bb = end bitmap bit next bit, e_oos = end out of sync bit 
	ULONG_PTR offset;
	ULONG_PTR bits, mask;
	int submit_count = 0;
	g_failed_unmarked = 0;
	g_unmarked_count = 0;

	s_bb = (ULONG_PTR)BM_SECT_TO_BIT(sector);
	e_next_bb = size == 0 ? s_bb : (ULONG_PTR)BM_SECT_TO_BIT(sector + (size >> 9));
	e_oos = 0;

	if (!list_empty(&device->marked_rl_list) || is_out_of_sync_after_replication(device, s_bb, e_next_bb)) {
		//int split_count = 0;
		g_count = 0;
		if (prepare_split_peer_request(peer_device, s_bb, e_next_bb, &g_count, &e_oos)) {
			printf("split peer_request sbb(%llu) ~ ebb(%llu), g_count(%d)\n", s_bb, (e_next_bb - 1), g_count);

			bool s_split_request = false;
			bool is_all_sync = (g_count == 0 ? true : false);
			struct drbd_peer_request *split_peer_req = NULL;
			struct drbd_marked_replicate *marked_rl;
			bool already_in_sync_bb = false;
			bool is_marked_bb = false;

			offset = s_bb;

			for (ULONG_PTR i_bb = offset = s_bb; i_bb < e_next_bb; i_bb++) {
				already_in_sync_bb = (drbd_bm_test_bit(peer_device, i_bb) == 0);
				is_marked_bb = is_marked_rl_bb(peer_device, &marked_rl, i_bb);

				if (is_marked_bb || already_in_sync_bb) {
					if (is_all_sync) {
						printf("all in sync bb sbb(%llu) ~ e_next(%llu)\n", s_bb, e_next_bb);
						return err;
					}

				submit_peer:
					//DW-1601 if offset is set to out of sync previously, write request to split_peer_req for data in index now from the corresponding offset.
					if (s_split_request) {

						split_read_in_block(peer_device, "sync bitmap", BM_BIT_TO_SECT(offset), (BM_BIT_TO_SECT(offset - s_bb) << 9),
							(unsigned int)(BM_BIT_TO_SECT(i_bb - offset) << 9), s_bb, e_next_bb, ((e_oos == (i_bb - 1) && !marked_rl) ? EE_SPLIT_LAST_REQ : EE_SPLIT_REQ));
						//split_peer_req = split_read_in_block(peer_device, peer_req,
						//	BM_BIT_TO_SECT(offset), (BM_BIT_TO_SECT(offset - s_bb) << 9),
						//	(unsigned int)(BM_BIT_TO_SECT(i_bb - offset) << 9),
						//	s_bb, e_next_bb,
						//	((e_oos == (i_bb - 1) && !marked_rl) ? EE_SPLIT_LAST_REQ : EE_SPLIT_REQ),
						//	split_count, NULL);

						//if (!split_peer_req) {
						//	drbd_err(peer_device, "failed to alloc split_peer_req, %llu\n", (unsigned long long)i_bb);
						//	err = -ENOMEM;
						//	goto split_error_clear;
						//}

						//list_add_tail(&split_peer_req->w.list, &peer_device->connection->sync_ee);

						//DW-1601, DW-1846 do not set out of sync unless it is a sync target.

						if (false) {
							//	err = -EIO;
							//	drbd_err(device, "submit failed, triggering re-connect\n");
						error_clear:
							//	spin_lock_irq(&device->resource->req_lock);
							//	list_del(&split_peer_req->w.list);
							//	spin_unlock_irq(&device->resource->req_lock);
							//	drbd_free_peer_req(split_peer_req);

							//DW-1601 If the drbd_submit_peer_request() fails, remove split_count - submit_count from the previously acquired split_cnt and turn off split_cnt if 0.
						split_error_clear:
							//DW-1923 for interparameter synchronization, an additional 1 was added for the remaining count and modified to use atomic_dec_return.
							//	atomic_set(split_count, atomic_read(split_count) - (atomic_read(split_count) - submit_count) + 1);
							//	if (split_count && 0 == atomic_dec_return(split_count))
							//		kfree2(split_count);
							//
							//	drbd_free_peer_req(peer_req);

							return err;
						}

						//DW-1601 submit_count is used for the split_cnt value in case of failure..
						submit_count += 1;
					}

					if (marked_rl) {

						//unmarked_count = kzalloc(sizeof(atomic_t), GFP_KERNEL, 'FFDW');
						//if (!unmarked_count) {
						//	drbd_err(peer_device, "failed to allocate unmakred_count\n");
						//	//DW-1923 to free allocation memory, go to the split_error_clean label.
						//	err = -ENOMEM;
						//	goto split_error_clear;
						//}

						//failed_unmarked = kzalloc(sizeof(atomic_t), GFP_KERNEL, 'FFDW');
						//if (!failed_unmarked) {
						//	drbd_err(peer_device, "failed to allocate failed_unmarked\n");
						//	//kfree2(unmarked_count);
						//	//DW-1923
						//	err = -ENOMEM;
						//	goto split_error_clear;
						//}

						//DW-1911 unmakred sector counting
						g_unmarked_count = (sizeof(marked_rl->marked_rl) * 8) - __popcnt(marked_rl->marked_rl);
						g_failed_unmarked = 0;

						for (int i = 0; i < sizeof(marked_rl->marked_rl) * 8; i++) {
							//DW-1911 perform writing per unmarked sector.
							if (!(marked_rl->marked_rl & 1 << i)) {

								split_read_in_block(peer_device, "unmarked", (BM_BIT_TO_SECT(marked_rl->bb) + i), ((BM_BIT_TO_SECT(marked_rl->bb - s_bb) + i) << 9),
									1 << 9, s_bb, e_next_bb, ((marked_rl->bb == e_oos && marked_rl->end_unmarked_rl == i) ? EE_SPLIT_LAST_REQ : EE_SPLIT_REQ));
								//split_peer_req = split_read_in_block(peer_device, peer_req,
								//	BM_BIT_TO_SECT(marked_rl->bb),
								//	((BM_BIT_TO_SECT(marked_rl->bb - s_bb) + i) << 9),
								//	1 << 9,
								//	s_bb, e_next_bb,
								//	((marked_rl->bb == e_oos && marked_rl->end_unmarked_rl == i) ? EE_SPLIT_LAST_REQ : EE_SPLIT_REQ),
								//	split_count, NULL);

								//								if (!split_peer_req) {
								//									drbd_err(peer_device, "failed to allocate split_peer_req, %llu\n", (unsigned long long)i_bb);
								//									unmarked_count = unmarked_count - (unmarked_count - submit_count) + 1;
								//									if (unmarked_count && 0 == (unmarked_count - 1)) {
								//										//kfree2(failed_unmarked);
								//										//kfree2(unmarked_count);
								//									}
								//
								//									err = -ENOMEM;
								//									goto split_error_clear;
								//								}

								//split_peer_req->unmarked_count = unmarked_count;
								//split_peer_req->failed_unmarked = failed_unmarked;
								//
								//list_add_tail(&split_peer_req->w.list, &peer_device->connection->sync_ee);
								//
								//atomic_add(split_peer_req->i.size << 9, &device->rs_sect_ev);

								//DW-1916 if you receive resync data from peer_device other than syncsource, set out of sync for peer_device except for the current syncsource.
								mask = bits = 99999999;

								//if (!is_sync_target(peer_device)) {
								//	struct drbd_peer_device* pd;
								//	for_each_peer_device(pd, device) {
								//		if (pd != peer_device && is_sync_target(pd)) {
								//			drbd_info(peer_device, "resync with other nodes in progress\n");
								//			clear_bit(pd->bitmap_index, &bits);
								//			clear_bit(pd->bitmap_index, &mask);
								//			break;
								//		}
								//	}
								//}

								// DW-1928 set out of sync for split_request.
								//drbd_set_sync(device, split_peer_req->i.sector, split_peer_req->i.size, bits, mask);

								if (false) {
									//drbd_err(device, "unmarked, submit failed, triggering re-connect\n");
									//DW-1923 for interparameter synchronization, an additional 1 was added for the remaining count and modified to use atomic_dec_return.
									g_unmarked_count = g_unmarked_count - (g_unmarked_count - submit_count) + 1;
									if (g_unmarked_count && 0 == (g_unmarked_count - 1)) {
										//	kfree2(failed_unmarked);
										//	kfree2(unmarked_count);
										//}
										err = -EIO;
										goto error_clear;
									}
								}

								submit_count += 1;
							}
						}
						g_unmarked_count = 0;
						g_failed_unmarked = 0;
					}

					// DW-1928 exit split because last out of sync request was made
					if (e_oos == (i_bb - 1))
						break;

					s_split_request = false;
				}
				else {
					if (s_split_request == false) {
						//DW-1601 set the first out of sync bit to offset.
						offset = i_bb;
						s_split_request = true;
					}

					if ((i_bb + 1) == e_next_bb) {
						i_bb += 1;
						goto submit_peer;
					}
				}
			}
		}
		else {
			peer_req.i.sector = sector;
			peer_req.i.size = size;
			peer_req.unmarked_count = NULL;
			peer_req.flags = 0;
			peer_req.peer_device = peer_device;

			fseek(g_fp, (peer_req.i.sector << 9), SEEK_SET);
			for (int i = 0; i < peer_req.i.size; i++)
				fwrite(RESYNC_DATA_STR, 1, 1, g_fp);

			split_e_end_resync_block(&peer_req);
		}
	}

	return err;
}

int drbd_bm_test_bit(struct drbd_peer_device* peer_device, ULONG_PTR bit)
{
	//printf("drbd_bm_test_bit() %llu => %s\n", bit, (bits & (1 << bit)) ? "out of sync" : "in sync");
	return ((g_bits & (1 << bit)) == 1 << bit);
}