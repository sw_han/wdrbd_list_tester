#include "common.h"

void atomic_inc(int *t)
{
	(*t)++;
}

#ifdef _WIN32
void __list_del(struct list_head * prev, struct list_head * next)
#else
static __inline void __list_del(struct list_head * prev, struct list_head * next)
#endif
{
#ifdef _WIN32
	if (prev == 0 || next == 0) {
		return;
	}
#endif
	next->prev = prev;
	prev->next = next;
}

#ifdef _WIN32
void list_del(struct list_head *entry)
#else
static __inline void list_del(struct list_head *entry)
#endif
{
#ifdef _WIN32
	if (entry == 0 || entry->prev == 0 || entry->next == 0) {
		return;
	}
#endif
	__list_del(entry->prev, entry->next);
	entry->next = 0;
	entry->prev = 0;
}

#ifdef _WIN32
void __list_add(struct list_head *n, struct list_head *prev, struct list_head *next)
#else
static __inline void __list_add(struct list_head *new, struct list_head *prev, struct list_head *next)
#endif
{
#ifdef _WIN32
	if (n == 0 || prev == 0 || next == 0) {
		return;
	}
#endif
	next->prev = n;
	n->next = next;
	n->prev = prev;
	prev->next = n;
}

#ifdef _WIN32
int list_empty(const struct list_head *head)
#else
static __inline int list_empty(const struct list_head *head)
#endif
{
#ifdef _WIN32
	if (head == 0) {
		return 1;
	}
#endif
	return head->next == head;
}

void list_add_tail(struct list_head *n, struct list_head *head)
{
	extern long *g_mdev_ptr, g_mdev_ptr_test;
	__list_add(n, head->prev, head);
}

#ifdef _WIN32
void list_add(struct list_head *n, struct list_head *head)
#else
static __inline void list_add(struct list_head *new, struct list_head *head)
#endif
{
#ifdef _WIN32
	if (n == 0 || head == 0) {
		return;
	}
#endif
	__list_add(n, head, head->next);
}

#ifdef _WIN32
void INIT_LIST_HEAD(struct list_head *list)
#else
static __inline void INIT_LIST_HEAD(struct list_head *list)
#endif
{
#ifdef _WIN32
	if (list == 0) {
		return;
	}
#endif
	list->next = list;
	list->prev = list;
}


void drbd_set_out_of_sync(struct drbd_peer_device *peer_device, sector_t sector, int size)
{
	printf("out of sync %llu ~ %llu\n", sector, sector + (size >> 9));
}

void drbd_set_in_sync(struct drbd_peer_device *peer_device, sector_t sector, int size)
{
	printf("in sync %llu ~ %llu\n", sector, sector + (size >> 9));
}

void drbd_rs_failed_io(struct drbd_peer_device *peer_device, sector_t sector, int size)
{
	printf("rs failed %llu ~ %llu\n", sector, sector + (size >> 9));
}

void drbd_info(void* object, const char* format, ...)
{
	va_list ap;
	char buf[MAX_BUF_SIZE];

	va_start(ap, format);
	vsprintf_s(buf, MAX_BUF_SIZE, format, ap);
	va_end(ap);
	fprintf(stdout, "%s, %s", __FUNCTION__, buf);
}

void drbd_err(void* object, const char* format, ...)
{
	va_list ap;
	char buf[MAX_BUF_SIZE];

	va_start(ap, format);
	vsprintf_s(buf, MAX_BUF_SIZE, format, ap);
	va_end(ap);
	fprintf(stdout, "%s, %s", __FUNCTION__, buf);
}

int _drbd_send_ack(struct drbd_peer_device *peer_device, enum drbd_packet cmd,
	ULONG_PTR sector, unsigned int blksize, ULONG_PTR block_id)
{
	printf("drbd send ack split(%s)\n", block_id == ID_SYNCER_SPLIT ? "ID_SYNCER_SPLIT" : "ID_SYNCER_DONE");
	return 0;
}
