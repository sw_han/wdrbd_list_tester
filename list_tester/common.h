#ifndef _COMMON_
#define _COMMON_

#include <stdlib.h>
#include <stdarg.h>
#include <iostream>
#include "stdafx.h"

//#if defined(_WIN64)
typedef __int64 INT_PTR, *PINT_PTR;
typedef unsigned __int64 UINT_PTR, *PUINT_PTR;

typedef __int64 LONG_PTR, *PLONG_PTR;
typedef unsigned __int64 ULONG_PTR, *PULONG_PTR;
#define __int3264   __int64
//#else
//typedef _W64 int INT_PTR, *PINT_PTR;
//typedef _W64 unsigned int UINT_PTR, *PUINT_PTR;
//
//typedef _W64 long LONG_PTR, *PLONG_PTR;
//typedef _W64 unsigned long ULONG_PTR, *PULONG_PTR;

//#define __int3264   __int32
//#endif


#define kfree2(x) if(x) delete x
#define ExAllocatePoolWithTag(x, y, z) new x
#define for_each_peer_device(x ,y) x = peer_device; if(x)

#define BM_BLOCK_SHIFT	12			 /* 4k per bit */
#define BM_SECT_TO_BIT(x)   ((x)>>(BM_BLOCK_SHIFT-9))

#define BM_BIT_TO_SECT(x)   ((sector_t)(x)<<(BM_BLOCK_SHIFT-9))

#define MAX_BUF_SIZE 4096

#define cpu_to_be64(x) x
#define cpu_to_be32(x) x

#define mutex_lock(x) x
#define mutex_unlock(x) x

#define ID_SYNCER_SPLIT 0
#define ID_SYNCER_SPLIT_DONE 1

#define EE_SPLIT_REQ		0
#define EE_SPLIT_LAST_REQ	1

#define UINT64_MAX       0xffffffffffffffffui64

#define container_of(ptr, type, member) \
	((type *)( \
	(char*)(ptr) - \
	(ULONG_PTR)(&((type *)0)->member)))

#define list_entry(ptr, type, member)		container_of(ptr, type, member)

#define list_for_each_entry(type, pos, head, member) \
	if((head)->next != NULL )	\
		for (pos = list_entry((head)->next, type, member);	\
				&pos->member != (head); 	\
				pos = list_entry(pos->member.next, type, member))

#define list_for_each_entry_safe(type, pos, n, head, member)                  \
		if((head)->next != NULL )	\
			for (pos = list_entry((head)->next, type, member),      \
						n = list_entry(pos->member.next, type, member); \
					&pos->member != (head);                                    \
					pos = n, n = list_entry(n->member.next, type, member))

#define atomic_t	int
#define BM_SECT_PER_BIT     BM_BIT_TO_SECT(1)
#define kfree(x) printf("kfree, %s\n", #x)
#define EE_WAS_ERROR 4

typedef unsigned short u16;

typedef ULONG_PTR sector_t;


#define IN_SYNC_STR "0"
#define OUT_OF_SYNC_STR "1"
#define RESYNC_DATA_STR "2"
#define RESYNC_PENDING_STR "3"
#define RESYNC_MARK_BIT_STR "4"

enum drbd_packet {
	P_NEG_ACK,
	P_RS_WRITE_ACK,
};

struct list_head {
	struct list_head *next, *prev;
};

struct drbd_resync_pending_sectors {
	sector_t sst;	/* start sector number */
	sector_t est;	/* end sector number */
	struct list_head pending_sectors;
};

struct drbd_device {
	list_head resync_pending_sectors;
	int resync_pending_fo_mutex;
	ULONG_PTR s_rl_bb;
	ULONG_PTR e_rl_bb;
	ULONG_PTR s_resync_bb;
	ULONG_PTR e_resync_bb;
	list_head marked_rl_list;
};
struct drbd_peer_device {
	struct drbd_device* device;
	ULONG_PTR rs_failed;
	ULONG_PTR current_uuid;
};


struct drbd_interval {
	sector_t sector;		/* start sector of the interval */
	unsigned int size;		/* size in bytes */
	sector_t end;			/* highest interval end in subtree */
	unsigned int local : 1		/* local or remote request? */;
	unsigned int waiting : 1;		/* someone is waiting for completion */
	unsigned int completed : 1;	/* this has been completed already;
								* ignore for conflict detection */
};

struct drbd_peer_request {
	struct drbd_interval i;
	unsigned int block_id;
	ULONG_PTR flags;
	struct drbd_peer_device *peer_device;

	struct {
		ULONG_PTR s_bb;		/* DW-1601 start bitmap bit of split data */
		ULONG_PTR e_next_bb;/* DW-1601 end next bitmap bit of split data  */
		atomic_t *count;	/* DW-1601 total split request (bitmap bit) */
		atomic_t *unmarked_count;	/* DW-1911 this is the count for the sector not written in the maked replication bit */
		atomic_t *failed_unmarked; /* DW-1911 true, if unmarked writing fails */
	};
};

struct drbd_marked_replicate {
	ULONG_PTR bb;	/* current bitmap bit */
	unsigned char marked_rl;	/* marks the sector as bit. (4k = 8sector = u8(8bit)) */
	struct list_head marked_rl_list;
	unsigned short end_unmarked_rl;
};



void atomic_inc(int *t);
void __list_del(struct list_head * prev, struct list_head * next);
void list_del(struct list_head *entry);
void __list_add(struct list_head *n, struct list_head *prev, struct list_head *next);
int list_empty(const struct list_head *head);
 void list_add_tail(struct list_head *n, struct list_head *head);
void list_add(struct list_head *n, struct list_head *head);
void INIT_LIST_HEAD(struct list_head *list);


void drbd_set_out_of_sync(struct drbd_peer_device *peer_device, sector_t sector, int size);
void drbd_set_in_sync(struct drbd_peer_device *peer_device, sector_t sector, int size);
void drbd_rs_failed_io(struct drbd_peer_device *peer_device, sector_t sector, int size);
void drbd_info(void* object, const char* format, ...);
void drbd_err(void* object, const char* format, ...);

int _drbd_send_ack(struct drbd_peer_device *peer_device, enum drbd_packet cmd,
	ULONG_PTR sector, unsigned int blksize, ULONG_PTR block_id);
#endif