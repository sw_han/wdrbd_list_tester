// list_tester.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#ifndef _LIST_TESTER_
#define _LIST_TESTER_

#include "split_request.h"


unsigned int g_bits;
FILE* g_fp;
int g_bits_length;

int test_verifier_file(drbd_peer_device* peer_device, unsigned int resync_sbb, unsigned int resync_ebb)
{
	printf("데이터 유형별로 쓰여지는 데이터는 아래와 같습니다\n");
	printf("IN SYNC(%s), OUT OF SYNC(%s), RESYNC DATA(%s), RESYNC PENDING(%s), REPLICATION MARK BIT(%s)\n\n",
		IN_SYNC_STR, OUT_OF_SYNC_STR, RESYNC_DATA_STR, RESYNC_PENDING_STR, RESYNC_MARK_BIT_STR);

	char d[256];

	for (int bi = 0; bi < g_bits_length; bi++) {
		if (g_bits & (1 << bi)) {
			for (int j = 0; j < 8; j++) {
				if (BM_BIT_TO_SECT(resync_sbb) <= ((bi * 8) + j) && BM_BIT_TO_SECT(resync_ebb) >((bi * 8) + j)) {
					memcpy(&d[(bi * 8) + j], RESYNC_DATA_STR, 1);
				}
				else
					memcpy(&d[(bi * 8) + j], OUT_OF_SYNC_STR, 1);
			}

			struct drbd_marked_replicate* marked_rl;

			list_for_each_entry(struct drbd_marked_replicate, marked_rl, &(peer_device->device->marked_rl_list), marked_rl_list) {
				if (marked_rl->bb == bi) {
					for (int j = 0; j < sizeof(marked_rl->marked_rl) * 8; j++) {
						if ((marked_rl->marked_rl & 1 << j)) {
							memcpy(&d[(bi * 8) + j], RESYNC_MARK_BIT_STR, 1);
						}
					}
				}
			}

			struct drbd_resync_pending_sectors* resync_pd;

			list_for_each_entry(struct drbd_resync_pending_sectors, resync_pd, &(peer_device->device->resync_pending_sectors), pending_sectors) {
				if (BM_SECT_TO_BIT(resync_pd->sst) <= bi &&
					BM_SECT_TO_BIT(resync_pd->est) > bi) {
					for (int j = 0; j < (resync_pd->est - resync_pd->sst); j++) 
						memcpy(&d[(bi * 8) + j], RESYNC_PENDING_STR, 1);
				}
			}
		}
		else {
			for (int j = 0; j < 8; j++) {
				d[(bi * 8) + j] = '0';
			}
		}
	}

	printf("설정 반영 후 섹터 별 데이터\n");
	for (int bi = 0; bi < g_bits_length; bi++) {
		for (int j = 0; j < 8; j++) {
			printf("sector[%02llu]=%c, ", (bi * 8) + j, d[(bi * 8) + j]);
		}
		printf("\n");
	}


	printf("파일에 쓰여진 섹터 별 데이터\n");
	char fd[512];
	int res = 0;

	fflush(g_fp);
	fseek(g_fp, 0, SEEK_SET);

	for (int bi = 0; bi < g_bits_length; bi++) {
		for (int j = 0; j < 8; j++) {
			res = fread(fd, 1, 512, g_fp);
			if (res != 512) {
				printf("error?\n");
			}

			printf("sector[%02llu]=%c, ", (bi * 8) + j, fd[0]);

			for (int fj = 0; fj < 512; fj++) {
				if (0 != memcmp(&d[(bi * 8) + j], &fd[fj], 1)) {
					printf("\n%02llu번째 섹터에 %d번째 데이터가 일치하지 않습니다. 설정(%c), 파일(%c)\n", (bi * 8) + j, fj, d[(bi * 8) + j], fd[fj]);
					return -1;
				}
			}

		}
		printf("\n");
	}

	printf("\n설정한 정보와 파일에 쓰여진 데이터가 일치 합니다.\n");

	return 0;
}

int test_resync_pending(struct drbd_peer_device *peer_device)
{
	struct drbd_device *device;
	struct drbd_peer_request peer_req;
	sector_t sst, est;
	unsigned int sbb, ebb;

	device = peer_device->device;
	peer_device->rs_failed = 0;

add_resync_pending:
	printf("적용 할 out of sync 영역을 섹터 단위로 입력 하세요. (없다면 0 0 입력) ");
	scanf_s("%llu %llu", &sst, &est);
	list_add_resync_pending(device, sst, est);

	if ((sst + est) != 0)
		goto add_resync_pending;

add_dedup:
	printf("복제가 발생한 영역을 섹터로 입력 하세요. (없다면 0 0 입력) ");
	scanf_s("%llu %llu", &sst, &est);
	dedup_from_resync_pending(peer_device, sst, est);

	if ((sst + est) != 0)
		goto add_dedup;

	printf("쓰기를 진행 할 동기화 데이터 비트맵 영역을 입력하세요. ");
	scanf_s("%u %u", &sbb, &ebb);
	printf("쓰기를 진행 할 동기화 데이터 영역 비트맵 %u ~ %u 비트 입니다.\n", sbb, ebb);
	split_recv_resync_read(peer_device, BM_BIT_TO_SECT(sbb), BM_BIT_TO_SECT(ebb - sbb) << 9);

	return test_verifier_file(peer_device, sbb, ebb);
}


int test_split_request(struct drbd_peer_device *peer_device)
{
	unsigned int sbb, ebb;
	printf("쓰기를 진행 할 동기화 데이터 비트맵 영역을 입력하세요. :");
	scanf_s("%u %u", &sbb, &ebb);
	printf("쓰기를 진행 할 동기화 데이터 영역 비트맵 %u ~ %u 비트 입니다.\n", sbb, ebb);
	split_recv_resync_read(peer_device, BM_BIT_TO_SECT(sbb), BM_BIT_TO_SECT(ebb - sbb) << 9);

	return test_verifier_file(peer_device, sbb, ebb);
}


int test_mark_replication(struct drbd_peer_device *peer_device)
{
	unsigned int sbb, ebb;
	sector_t sst, est;

add_mark:
	printf("복제가 발생한 영역에 섹터 범위를 입력 하세요. (없다면 0 0 입력) ");
	scanf_s("%llu %llu", &sst, &est);

	list_add_marked(peer_device, sst, est, (unsigned int)((est - sst) << 9), true);

	if ((sst + est) != 0)
		goto add_mark;

	printf("쓰기를 진행 할 동기화 데이터 비트맵 영역을 입력하세요. ");
	scanf_s("%u %u", &sbb, &ebb);

	printf("쓰기를 진행 할 동기화 데이터 영역 비트맵 %u ~ %u 비트 입니다.\n", sbb, ebb);
	split_recv_resync_read(peer_device, BM_BIT_TO_SECT(sbb), BM_BIT_TO_SECT(ebb - sbb) << 9);

	return test_verifier_file(peer_device, sbb, ebb);
}

int _tmain(int argc, _TCHAR* argv[])
{
	struct drbd_device device;
	struct drbd_peer_device peer_device;
	char bitmap[(sizeof(g_bits) * 8 + 1)];
	int testcase = 0;
	int res = 0;

	peer_device.device = &device;
	device.s_resync_bb = 0;
	device.e_resync_bb = 256;

	INIT_LIST_HEAD(&peer_device.device->marked_rl_list);
	INIT_LIST_HEAD(&peer_device.device->resync_pending_sectors);

	device.s_rl_bb = 0;
	device.e_rl_bb = -1;
restart:
	g_bits = 0;
	g_bits_length = 0;

	struct drbd_resync_pending_sectors *peding, *tp;
	list_for_each_entry_safe(struct drbd_resync_pending_sectors, peding, tp, &(peer_device.device->resync_pending_sectors), pending_sectors) {
		list_del(&peding->pending_sectors);
		kfree2(peding);
	}

	struct drbd_marked_replicate* marked, *tm;
	list_for_each_entry_safe(struct drbd_marked_replicate, marked, tm, &(peer_device.device->marked_rl_list), marked_rl_list) {
		list_del(&marked->marked_rl_list);
		kfree2(marked);
	}

	errno_t error = fopen_s(&g_fp, "ssss", "w+");
	if (error == 0) {
		printf("비트맵은 최대 32bit 크기를 지원하며 2진수로 입력해주세요. ");
		scanf_s("%s", bitmap, sizeof(bitmap));
		while (bitmap[g_bits_length] != NULL) {
			g_bits_length++;
			if (g_bits_length > sizeof(g_bits) * 8) {
				printf("입력한 비트맵의 크기가 32bit를 넘어 갔습니다. 다시 시도해주세요.\n");
				return 0;
			}
		}

		for (int i = 0; i < g_bits_length; i++){
			if (bitmap[i] % 2 == 0) {
				fseek(g_fp, ((BM_BIT_TO_SECT(1) << 9) * i), SEEK_SET);
				for (int j = 0; j < (BM_BIT_TO_SECT(1) << 9); j++)
					fwrite(IN_SYNC_STR, 1, 1, g_fp);
				printf("비트맵 %d, in sync 설정\n", i);
			}
			else {
				g_bits += (1 << i);
				fseek(g_fp, ((BM_BIT_TO_SECT(1) << 9) * i), SEEK_SET);
				for (int j = 0; j < (BM_BIT_TO_SECT(1) << 9); j++)
					fwrite(OUT_OF_SYNC_STR, 1, 1, g_fp);
				printf("비트맵 %d, out of sync 설정\n", i);
			}
		}

		printf("비트맵 (%u), 진행 할 테스트를 설정 해주세요. 0(resync pending), 1(split request), 2(mark replication bit) ", g_bits);
		scanf_s("%d", &testcase);

		switch (testcase) {
		case 0:
			res = test_resync_pending(&peer_device);
			break;
		case 1:
			res = test_split_request(&peer_device);
			break;
		case 2:
			res = test_mark_replication(&peer_device);
			break;
		default:
			printf("진행 할 수 없는 테스트 입니다. 0(resync pending), 1(split request), 2(mark replication bit)\n");
			return -1;
		}
	}
	else
		printf("테스트 파일을 생성하는데 실패 했습니다.\n");


	fclose(g_fp);

	goto restart;

	return res;
}

#endif